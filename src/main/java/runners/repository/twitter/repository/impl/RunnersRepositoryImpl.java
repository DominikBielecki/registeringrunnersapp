package runners.repository.twitter.repository.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import runners.repository.RunnersRepository;
import runners.runners.model.Runner;

import java.util.Date;
import java.util.List;

public class RunnersRepositoryImpl implements RunnersRepository {

    private SessionFactory sessionFactory;


    public RunnersRepositoryImpl() {
        initRepo();
    }

    public void initRepo() {
        Configuration config = new Configuration().configure();
        config.addAnnotatedClass(Runner.class);
        ServiceRegistry service = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
        sessionFactory = config.buildSessionFactory(service);
        test1();
        test2();
    }

    private void test1() {
        Session session = sessionFactory.openSession();
        Runner runner1 = new Runner("Dominik", "Bielecki", "1234", new Date());
        session.beginTransaction();
        session.save(runner1);
        session.getTransaction().commit();
    }
    private void test2() {
        Session session = sessionFactory.openSession();
        Runner runner1 = new Runner("Jan", "Nowak", "5678", new Date());
        session.beginTransaction();
        session.save(runner1);
        session.getTransaction().commit();
    }


    public List<Runner> getAllRunners() {
        Session session = sessionFactory.openSession();
        List<Runner> runners = session.createQuery("from Runner").list();
        return runners;
    }

    public Runner persistRunner(Runner runner) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(runner);
        session.getTransaction().commit();
        return runner;
    }
}