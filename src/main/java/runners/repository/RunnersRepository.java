package runners.repository;

import runners.runners.model.Runner;

import java.util.List;

public interface RunnersRepository {

    List<Runner> getAllRunners();

    Runner persistRunner(Runner runner);

}
