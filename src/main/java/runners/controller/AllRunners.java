package runners.controller;

import runners.runners.model.Runner;
import runners.service.RunnerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "AllRunners", value = "/allRunners")
public class AllRunners extends HttpServlet {

    private RunnerService runnerService = new RunnerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Runner> runners = runnerService.getAllRunners();
        req.setAttribute("runner", runners);
        RequestDispatcher dispatcher = req.getRequestDispatcher("allRunners.jsp");
        dispatcher.forward(req, resp);

    }
}
