package runners.controller;

import runners.repository.twitter.repository.impl.RunnersRepositoryImpl;
import runners.service.RunnerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet(name = "AddRunner", value ="/addRunner")
public class AddRunnerController extends HttpServlet {

    private RunnerService service = new runners.service.RunnerService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = req.getRequestDispatcher("addRunner.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String surname = req.getParameter("surname");
        String password = req.getParameter("password");
        if (userName != null && userName !="" && surname != null && surname != "" && password != null && password != "") {
         service.addRunner(userName, surname, password);
        }
        if (userName != null && surname != null && password != null) {
            HttpSession session = req.getSession();
            session.setAttribute("userName", userName);
            session.setAttribute("surname", surname);
            resp.sendRedirect("/runners/hello");
        } else {
            resp.getWriter().println("Wrong inputs");
        }
    }
}