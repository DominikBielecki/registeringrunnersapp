package runners.runners.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "Runners")
public class Runner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull
    private String userName;
    @NotNull
    private String surname;
    @NotNull
    private String password;
    @NotNull
    private Date dateOfAddingRunner;

    public Runner(String userName, String surname, String password, Date dateOfAddingRunner ) {
        this.userName = userName;
        this.surname = surname;
        this.password = password;
        this.dateOfAddingRunner = dateOfAddingRunner;
    }

    public Runner(){

    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getId() {

        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getSurname() {
        return surname;
    }

    public String getPassword() {
        return password;
    }

    public Date getDateOfAddingRunner() {
        return dateOfAddingRunner;
    }

    public void setDateOfAddingRunner(Date dateOfAddingRunner) {
        this.dateOfAddingRunner = dateOfAddingRunner;
    }
}
