package runners.service;

import runners.repository.RunnersRepository;
import runners.repository.twitter.repository.impl.RunnersRepositoryImpl;
import runners.runners.model.Runner;

import java.util.Date;
import java.util.List;

public class RunnerService {

    private RunnersRepository runnersRepositoryImpl = new RunnersRepositoryImpl();

    public List<Runner> getAllRunners() {
        return runnersRepositoryImpl.getAllRunners();
    }



    public void addRunner(String userName, String surname, String password){
        Runner runner = new Runner(userName, surname, password, new Date());
        runnersRepositoryImpl.persistRunner(runner);
    }

}

