<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div>
<c:if test="${not empty sessionScope.userName}">
	<b>Hello ${sessionScope.userName} ${sessionScope.surname} now you're registered.</b>

	</div>
	<form method="get" action="/runners/allRunners">
		<legend>If you want to check registered runners click button below:</legend>

		<button type="submit" class="btn btn-dark">
			<i class="icon-user icon-white"></i> Check
		</button>

	</form>
	
</c:if>
</div>
<div>
<c:if test="${empty sessionScope.userName}">
	<b>Invalid inputs</b>


</c:if>
</div>