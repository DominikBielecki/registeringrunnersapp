<%@ page language="java" contentType="text/html;" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <head>
        <meta charset="UTF-8">
        <title>AppForRunners</title>
        <meta name="description" content="">
        <meta name="keywords" content="front, www, html">
        <link rel="stylesheet" href="style.css" />

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">
    </head>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
<header>
    <div>
        <div class="header-title">
            run with us
        </div>
        <div class="header-subtitle">
            run and stay healthy
        </div>
    </div>
    <nav>
        <div>
            <a class="active" href="index.jsp">Home</a>
        </div>
        <div>
                <a href="/runners/allRunners">List of runners</a>
        </div>
        <div>
            <a href="addRunner.jsp">Registration</a>
        </div>
    </nav>
</header>
<div class="section">
    <h1 class="section-title" style="color: black">Run often. Run long. But never outrun your joy of running</h1>
    <div class="flex">
        <div class="box small-box">
            <div class="box-content">
                <div style="background-image: url(mapa.jpg);" class="image-wrapper">

                </div>
            </div>
        </div>
        <div class="box big-box">
            <div class="box-content">
                <p class="box-header">
                    Information about run
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vitae mi sit amet libero finibus rutrum. Duis id nisi sit amet orci semper egestas ac eget leo. Nulla non tortor lectus. Pellentesque consequat eros nibh, et lobortis quam egestas vitae. Sed fermentum viverra pharetra. Nunc luctus vulputate sapien eu vehicula. Sed nec lorem id lectus egestas egestas. Quisque pharetra quam vel purus dignissim, eget ornare quam fringilla. Etiam ac nisi risus. Mauris dapibus nulla nulla, vel euismod elit aliquet ac. Curabitur vel dolor elementum, consectetur eros ac, porttitor urna. Proin sed metus ut justo lobortis semper. Nam ullamcorper viverra lectus et laoreet. Donec et congue nisi. Etiam quam elit, imperdiet eget condimentum ut, blandit nec odio.
                </p>
                <div style="text-align: center">
                    <form method="get" action="/runners/addRunner">
                        <legend>If you want to register just click button below:</legend>

                        <button type="submit" class="btn btn-dark">
                            <i class="icon-user icon-white"></i> Quick register
                        </button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>