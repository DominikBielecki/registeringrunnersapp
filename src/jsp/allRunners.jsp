<%@ page language="java" contentType="text/html;" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>All Registered Runners</title>
    <meta name="description" content="">
    <meta name="keywords" content="front, www, html">

    <link rel="stylesheet" href="style.css" />

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<header>
    <div style="background-image: url(allRunners.jpg)">
        <div class="header-title">
            Win or lose, I believe in giving my best and that is what i always do.
        </div>
        <div class="header-subtitle">
        </div>
    </div>
    <nav>
        <div>
            <a href="index.jsp">Home</a>
        </div>
        <div>
            <a class="active" href="/runners/allRunners">List of runners</a>
        </div>
        <div>
            <a href="addRunner.jsp">Registration</a>
        </div>
    </nav>
</header>
<h1>Runners List : </h1>
<div>
<c:forEach items="${runner}" var="runner">

<p>Added: ${runner.userName} ${runner.surname}  on ${runner.dateOfAddingRunner}</p>

</c:forEach>
</div>
</body>
</html>