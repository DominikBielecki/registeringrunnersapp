<%@ page language="java" contentType="text/html;" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>addRunner</title>
    <meta name="description" content="">
    <meta name="keywords" content="front, www, html">

    <link rel="stylesheet" href="style.css" />

    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<header>
    <div style="background-image: url(addRunner2.jpg)">
        <div class="header-title">
            registration for free run
        </div>
        <div class="header-subtitle">
            run and stay healthy
        </div>
    </div>
    <nav>
        <div>
            <a href="index.jsp">Home</a>
        </div>
        <div>
            <a href="/runners/allRunners">List of runners</a>
        </div>
        <div>
            <a class="active" href="addRunner.jsp">Registration</a>
        </div>
    </nav>
</header>

<h1 style="text-align: center">Register yourself for free! </h1>
<div class="form">
    <form method="post" action="/runners/addRunner">
        <legend>Add information about you:</legend>
        <br>
        <input class="form-control" type="text" name="userName" placeholder="Name">
        <br>
        <input class="form-control" type="text" name="surname" placeholder="Surname">
        <br>
        <input class="form-control" type="password" name="password" placeholder="Password">
        <br>
        <button type="submit" class="btn btn-dark">
            <i class="icon-user icon-white"></i> Send
        </button>
    </form>
</div>
</body>


</html>